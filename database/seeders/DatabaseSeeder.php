<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Course;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Database\Seeder;
use PHPUnit\Framework\MockObject\Builder\Stub;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //*****************************************************/
        //**** SORRY FOT THE BAD WHY OF SEEDING AND NOT USING */
        //**** FACTORIES OR JSON FILES ************************/
        //*****************************************************/

        $students = Student::factory(10)->create();

        $admin = User::create([
            'username' => 'majd',
            'slug' => 'majd',
            'email' => 'majdshatti@gmail.com',
            'password' => '$2y$10$rAT8lxjqEtbL30y3E5b65Ot.CgeASF4K1FvD6jJW0BNVixZewr.x6',
            '_id' => '97d827b1073e41e5bbefbda6f037f6fd'
        ]);

        $itSubject = Subject::create([
            '_id' => '97d8290efcc941db91f6051bc77a324c',
            'name' => 'IT',
            'slug' => 'it',
        ]);

        $apdSubject = Subject::create([
            '_id' => '97d8290efcc941db91f6051bc77a325c',
            'name' => 'APD',
            'slug' => 'apd',
        ]);

        $nlpSubject = Subject::create([
            '_id' => '97d8290efcc941db91f6051bc77a326c',
            'name' => 'NLP',
            'slug' => 'nlp',
        ]);

        $saeedTeacher = Teacher::create([
            '_id' => '97d8290efcc941db91f6051bc77a225c',
            'name' => 'Saeed',
            'slug' => 'saeed',
            'age' => 20
        ]);

        $tarekTeacher = Teacher::create([
            '_id' => '97d8290efcc941db91f6051bc77a226c',
            'name' => 'Tarek',
            'slug' => 'tarek',
            'age' => 32
        ]);

        $jhinTeacher = Teacher::create([
            '_id' => '97d8290efcc941db91f6051bc77a227c',
            'name' => 'Jhin',
            'slug' => 'jhin',
            'age' => 45
        ]);

        $itSaeed = Course::create([
            '_id' => '97d8290efcc951db91f6051bc77a228c',
            'teacher_id' => $saeedTeacher->id,
            'subject_id' => $itSubject->id,
            'limit' => 5
        ]);

        $itJhin = Course::create([
            '_id' => '97d8290efcc951db91f6051bc77a229c',
            'teacher_id' => $jhinTeacher->id,
            'subject_id' => $itSubject->id,
            'limit' => 5
        ]);

        $itTarek = Course::create([
            '_id' => '97d8290efcc951db91f6051bc77a230c',
            'teacher_id' => $tarekTeacher->id,
            'subject_id' => $itSubject->id,
            'limit' => 5
        ]);

        $apdSaeed = Course::create([
            '_id' => '97d8290efcc951db91f6051bc77a231c',
            'teacher_id' => $saeedTeacher->id,
            'subject_id' => $apdSubject->id,
            'limit' => 5
        ]);

        $apdJhin = Course::create([
            '_id' => '97d8290efcc951db91f6051bc77a232c',
            'teacher_id' => $jhinTeacher->id,
            'subject_id' => $apdSubject->id,
            'limit' => 5
        ]);

        $apdTarek = Course::create([
            '_id' => '97d8290efcc951db91f6051bc77a233c',
            'teacher_id' => $tarekTeacher->id,
            'subject_id' => $apdSubject->id,
            'limit' => 5
        ]);

        $nlpSaeed = Course::create([
            '_id' => '97d8290efcc951db91f6051bc77a234c',
            'teacher_id' => $saeedTeacher->id,
            'subject_id' => $nlpSubject->id,
            'limit' => 5
        ]);

        $nlpTarek = Course::create([
            '_id' => '97d8290efcc951db91f6051bc77a235c',
            'teacher_id' => $tarekTeacher->id,
            'subject_id' => $nlpSubject->id,
            'limit' => 5
        ]);

        $nlpJhin = Course::create([
            '_id' => '97d8290efcc951db91f6051bc77a236c',
            'teacher_id' => $jhinTeacher->id,
            'subject_id' => $nlpSubject->id,
            'limit' => 5
        ]);
    }
}
