<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


require_once 'api-routes/auth.php';
require_once 'api-routes/subject.php';
require_once 'api-routes/teacher.php';
require_once 'api-routes/student.php';
require_once 'api-routes/course.php';
