<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

/** Private Routes */
Route::group(["middleware" => ["auth:sanctum"]], function () {
    Route::post('student/{slug}/register/course', [StudentController::class, 'registerCourse']);
    Route::post('student/{slug}/drop/course', [StudentController::class, 'dropCourse']);
    Route::apiResource('student', StudentController::class);
});
