<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CourseController;

/** Private Routes */
Route::group(["middleware" => ["auth:sanctum"]], function () {
    Route::group(["prefix" => "course"], function () {
        Route::get('/', [CourseController::class, 'index']);
        Route::post('/', [CourseController::class, 'store']);
        Route::delete('/{id}', [CourseController::class, 'destroy']);
    });
});
