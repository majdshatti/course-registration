<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SubjectController;

/** Private Routes */
Route::group(["middleware" => ["auth:sanctum"]], function () {
    Route::apiResource('subject', SubjectController::class);
});
