<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TeacherController;

/** Private Routes */
Route::group(["middleware" => ["auth:sanctum"]], function () {
    Route::group(["prefix" => "teacher"], function () {
        Route::get('/', [TeacherController::class, 'index']);
        Route::post('/', [TeacherController::class, 'store']);
    });
});
