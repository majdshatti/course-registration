<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;


/** Public routes */
Route::post("/register", [RegisterController::class, "store"]);
Route::post("/login", [LoginController::class, "index"]);
