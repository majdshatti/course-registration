<!DOCTYPE html>
<head>
  <title>Notifications</title>
  <link rel="stylesheet" href="{{ asset('notification.css') }}">
  <script src="https://js.pusher.com/7.2/pusher.min.js"></script>
  <script src="{{ asset('notification.js') }}"></script>
</head>

<body>
    <div class="flex justify-center outter-container">
        <div class="container">
            <h1 class="header">Courses Notifications</h1>
            <div class="flex flex-col box-container" id="container">

            </div>
        </div>
    </div>
</body>

