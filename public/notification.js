Pusher.logToConsole = true;

var pusher = new Pusher("569aad112cffb5254f0c", {
    cluster: "eu",
});

var channel = pusher.subscribe("notification");

channel.bind("courseIsFull", function (data) {
    var container = document.getElementById("container");

    container.insertAdjacentHTML(
        "afterbegin",
        `<div class="noti-box flex flex-col">
            <div class="noti-header flex flex-col">
                <div>${data.subjectName ?? "course"} of Teacher: ${
            data.teacherName ?? "teacher"
        }</div>
                <div class="noti-date">${data.date ?? "00:00:00"}</div>
            </div>
            <div class="noti-body"> ${data.message ?? "message"} </div>
        </div>`
    );
});
