<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Throwable;

use function PHPUnit\Framework\returnSelf;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        $exception = $this->prepareException($exception);

        Log::info($exception->getMessage());

        if($exception instanceof ResourceNotFoundException ||
            $exception instanceof RouteNotFoundException
        ){
            if($exception->getMessage() === "Route [login] not defined.")
                return exceptionResponse(401, "Unauthorized");
            return exceptionResponse(404, "Resource Not Found");
        }

        // Reform validation exceptions
        if ($exception instanceof ValidationException) {
            return exceptionResponse($exception->status, $exception->getMessage(), $exception->errors());
        }

        // Custom Http exceptions
        if ($exception instanceof HttpException) {
            if(!$exception->getMessage())
                return exceptionResponse(404, "Resource not found");
            return exceptionResponse($exception->getStatusCode(), $exception->getMessage());
        }

        // Authentication & Authorization exceptions
        if (
            $exception instanceof AuthenticationException ||
            $exception instanceof AuthorizationException
        ) {
            return exceptionResponse(401, $exception->getMessage());
        }

        // Unhandled exceptions
        if ($exception) {
            // Log::channel("exception")->info($exception->getMessage());
            return exceptionResponse(500, "Internal Server Error");
        }

        return parent::render($request, $exception);
    }
}
