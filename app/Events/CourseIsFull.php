<?php

namespace App\Events;

use App\Models\Course;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CourseIsFull implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The order instance.
     *
     * @var \App\Course
     */
    public $course;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($course)
    {
        $this->course = $course;
    }

    /**
     * Get the channels the event should broadcast on.
     * Note: number 1 indicates that in this system there is only on user,
     *       whos the one is registering students
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('notification');
    }

    public function broadcastAs()
    {
        return 'courseIsFull';
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return
        [
            'subjectName' => $this->course->subject->name,
            'teacherName' => $this->course->teacher->name,
            'date' => $this->course->created_at,
            'message' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur minima exercitationem maxime assumenda ducimus, eum libero provident quisquam temporibus reprehenderit odio ipsam dolorem consequuntur repellat ratione facere porro numquam quod?'
        ];
    }
}
