<?php

use Illuminate\Support\Str;

/**
 * Generates a uuid
 *
 * @return string uuid
 */
if (!function_exists("generateUuid")) {
    function generateUuid()
    {
        return Str::orderedUuid()->getHex();
    }
}

?>
