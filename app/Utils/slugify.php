<?php

use Illuminate\Support\Str;

/**
 * Gives a slug version of a string
 *
 * @return String slug
 */
if (!function_exists("slugify")) {
    function slugify($string)
    {
        return Str::slug($string);
    }
}

?>
