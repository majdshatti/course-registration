<?php

/**
 * Returns error response to the client.
 *
 * @param Throwable $exception An instance of throwable exception class.
 *
 * @return JsonResponse
 */

use Illuminate\Support\Facades\Log;

if (!function_exists("exceptionResponse")) {
    function exceptionResponse(int $statusCode, string $customMessage, array $errors = null)
    {
        $response = [];
        $response["message"] = $customMessage;

        if(isset($errors))
            $response["errors"] = $errors;

        $response["success"] = false;

        // Return json response
        return response($response, $statusCode);
    }
}

?>
