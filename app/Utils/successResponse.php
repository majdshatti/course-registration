<?php

/**
 * Returns a united response for all successful routes
 *
 * @return Response
 */
if (!function_exists("successResponse")) {
    function successResponse(array $data, int $statusCode = 200)
    {
        $data['success'] = true;

        return response($data, $statusCode);
    }
}

?>
