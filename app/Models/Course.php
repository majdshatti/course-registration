<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $table = "courses";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [ '_id', 'limit', 'subject_id', 'teacher_id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['id'];

    /**
     * Relation with subject, each course belongs to subject
     */
    public function subject() {
        return $this->belongsTo(Subject::class);
    }

    /**
     * Relation with teacher, each course belongs to teacher
     */
    public function teacher() {
        return $this->belongsTo(Teacher::class);
    }

    /**
     * Relation with student, (many to many)
     */
    public function students(){
        return $this->belongsToMany(Student::class, 'course_student')
            ->withPivot('status')
            ->withTimestamps();
    }
}
