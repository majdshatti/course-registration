<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    /**
     * The table name of the model.
     *
     * @var string
     */
    public $table = "students";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [ '_id', 'name', 'slug'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['id'];

    public function courses(){
        return $this->belongsToMany(Course::class, 'course_student')
            ->withPivot('status')
            ->withTimestamps();
    }

    public function scopeFilter($query, array $filters)
    {
        // Filerting on courseId
        $query->when($filters["courseId"] ?? false,
            fn($query, $courseId) => $query->whereHas('courses',
                fn($query) => $query->where('_id', '=', $courseId)
            )
        );

        // Filtering on teachers that age is greater than specified value
        $query->when($filters["teacherAgeGT"],
            fn($query, $value) => $query->whereHas('courses',
                fn($query) => $query->whereHas('teacher',
                    fn($query) =>
                        $query->where('age', '>', (int) $value)
                )
            )
        );
    }
}
