<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;

    /**
     * The table name of the model.
     *
     * @var string
     */
    public $table = "teachers";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [ '_id', 'name', 'slug', 'age'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['id'];

    /**
     * Relation with courses, teacher can take multiple courses
     */
    protected function courses() {
        return $this->hasMany(Course::class);
    }
}
