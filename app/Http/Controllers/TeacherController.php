<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use App\Http\Requests\Teacher\StoreRequest;

class TeacherController extends Controller
{
    /**
     * Display a listing of the teachers.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return successResponse(["data" => Teacher::all()]);
    }

    /**
     * Store a newly created teacher in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $attributes = $request->validated();

        $teacherData = array_merge($attributes, [
            '_id' => generateUuid(),
            'slug' => slugify($attributes['name']),
        ]);

        $teacher = Teacher::create($teacherData);

        return successResponse([
            "data" => $teacher,
            "message" => "teacher created successfully"
        ], 201);
    }
}
