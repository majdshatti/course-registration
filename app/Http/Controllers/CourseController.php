<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Http\Requests\Course\StoreRequest;
use App\Models\Subject;
use App\Models\Teacher;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CourseController extends Controller
{
    /**
     * Display a listing of the subjects.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return successResponse(["data" => Course::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $attributes = $request->validated();

        $subject = Subject::where('_id', $attributes['subjectId'])->first();
        $teacher = Teacher::where('_id', $attributes['teacherId'])->first();

        $courseData = array_merge($attributes, [
            '_id' => generateUuid(),
            'subject_id' => $subject->id,
            'teacher_id' => $teacher->id
        ]);

        $course = Course::create($courseData);

        return successResponse(["data" => $course], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::where('_id', $id)->first();

        if(!$course) throw new NotFoundHttpException('course does not exist');

        $course->delete();

        return successResponse([
            "message" => "course deleted successfully"
        ]);
    }
}
