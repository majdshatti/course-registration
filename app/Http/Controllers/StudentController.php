<?php

namespace App\Http\Controllers;

use App\Events\CourseRegistered;
use App\Http\Requests\Student\RegisterRequest;
use App\Http\Requests\Student\StoreRequest;
use App\Models\Course;
use App\Models\Student;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return successResponse([
            "data" => Student::filter(request(['courseId', 'teacherAgeGT']))->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $attributes = $request->validated();

        $studentData = array_merge($attributes, [
            '_id' => generateUuid(),
            'slug' => slugify($attributes['name']),
        ]);

        $student = Student::create($studentData);

        return successResponse([
            "data" => $student,
            "message" => "student created successfully"
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $student = Student::where('slug', $slug)->first();

        if(!$student) throw new NotFoundHttpException('student does not exist');

        return successResponse(["data" => $student]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $slug)
    {
        $student = Student::where('slug', $slug)->first();

        if(!$student) throw new NotFoundHttpException('student does not exist');

        $student->name = $request['name'] ?? $student->name;
        $student->slug =
            $request['name'] ? slugify($request['name']) : $student->slug;

        $student->save();

        return successResponse([
            "data" => $student,
            "message" => "student updated successfully"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $student = Student::where('slug', $slug)->first();

        if(!$student) throw new NotFoundHttpException('student does not exist');

        $student->delete();

        return successResponse([
            "message" => "student deleted successfully"
        ]);
    }

    /**
     * Register the specified course to the student.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function registerCourse(RegisterRequest $request, $slug) {
        $student = Student::where('slug', $slug)->first();
        $course = Course::where('_id', $request['courseId'])->first();

        if(!$student) throw new NotFoundHttpException('student does not exist');

        // Check if course is already given to the user and already active not dropped
        $isGiven = DB::table('course_student')
            ->where('course_id', '=', $course->id)
            ->where('student_id', '=', $student->id)
            ->where('status', '=', 'active')
            ->first();

        if($isGiven)
            throw new BadRequestHttpException('Course is already given to the user');

        $student->courses()->attach($course->id);

        event(new CourseRegistered($course));

        return successResponse(["message" => "course registered"]);
    }

    /**
     * Drop the specified course for the student.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function dropCourse(RegisterRequest $request, $slug) {
        $student = Student::where('slug', $slug)->first();
        $course = Course::where('_id', $request['courseId'])->first();

        if(!$student) throw new NotFoundHttpException('student does not exist');

        // Check if course is already given to the user and already active not dropped
        $isDropped = DB::table('course_student')
            ->where('course_id', '=', $course->id)
            ->where('student_id', '=', $student->id)
            ->update(["status" => "dropped"]);

        if(!$isDropped)
            throw new NotFoundHttpException('course does not exist or it\'s already dropped');

        return successResponse(["message" => "success"]);
    }
}
