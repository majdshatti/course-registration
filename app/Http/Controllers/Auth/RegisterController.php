<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * Registers user and returns a token
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function store(RegisterRequest $request) {
        $attributes = $request->validated();

        $user = User::create([
            '_id' => generateUuid(),
            'username' => $attributes['username'],
            'slug' => slugify($attributes['username']),
            'email' => $attributes['email'],
            'password' => Hash::make($attributes['password']),
        ]);

        return successResponse([
            "message" => 'user registered successfully',
        ], 201);
    }
}
