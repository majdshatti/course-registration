<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LoginController extends Controller
{
    /**
     * Logins a user
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function index(LoginRequest $request) {
        $attributes = $request->validated();

        // Fetch logging user
        $user = User::where("email", $attributes['email'])->first();

        if (!$user) throw new NotFoundHttpException('Invalid credentials');

        // Checking encrypted user password
        if (!Hash::check($attributes['password'], $user->password))
            throw new NotFoundHttpException('Invalid credentials');

        // Register token for the user
        $token = $user->createToken("API TOKEN")->plainTextToken;

        return successResponse([
            "user" => $user,
            "token" => $token
        ]);
    }
}
