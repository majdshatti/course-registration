<?php

namespace App\Http\Controllers;


use App\Models\Subject;
use App\Http\Requests\Subject\StoreRequest;
use App\Http\Requests\Subject\UpdateRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SubjectController extends Controller
{
    /**
     * Display a listing of the subjects.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return successResponse(["data" => Subject::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $attributes = $request->validated();

        $subjectData = array_merge($attributes, [
            '_id' => generateUuid(),
            'slug' => slugify($attributes['name']),
        ]);

        $subject = Subject::create($subjectData);

        return successResponse([
            "data" => $subject,
            "message" => "subject created successfully"
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $subject = Subject::where('slug', $slug)->first();

        if(!$subject) throw new NotFoundHttpException('subject does not exist');

        return successResponse(["data" => $subject]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $slug)
    {
        $subject = Subject::where('slug', $slug)->first();

        if(!$subject) throw new NotFoundHttpException('subject does not exist');

        $subject->name = $request['name'] ?? $subject->name;
        $subject->slug =
            $request['name'] ? slugify($request['name']) : $subject->slug;

        $subject->save();

        return successResponse([
            "data" => $subject,
            "message" => "subject updated successfully"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $subject = Subject::where('slug', $slug)->first();

        if(!$subject) throw new NotFoundHttpException('subject does not exist');

        $subject->delete();

        return successResponse([
            "message" => "subject deleted successfully"
        ]);
    }
}
