<?php

namespace App\Listeners;

use App\Events\CourseIsFull;
use App\Events\CourseRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CheckCourseLimit
{
    /**
     * Handle the event.
     *
     * @param  \App\Events\CourseRegistered  $event
     * @return void
     */
    public function handle(CourseRegistered $event)
    {
        $course = $event->course;

        $registeredStudents = $course->students()->wherePivot('status', '=', 'active')->count();

        if($registeredStudents == $course->limit)
            CourseIsFull::dispatch($course);
    }
}
